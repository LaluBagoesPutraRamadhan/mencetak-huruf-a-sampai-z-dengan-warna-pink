.MODEL SMALL
.STACK 100H
.DATA
    CAP DB 'A'
.CODE


MAIN PROC
    MOV AX, @DATA
    MOV DS,AX
    
    PRINT:
    MOV AH, 09H
    MOV BL, 0DH
    MOV CX,01H
    INT 10H
    
    
    MOV AH,2
    MOV DL, CAP
    INT 21H
    
    
    CMP DL, 'Z'
    JGE EXIT
    
    INC CAP
    JMP PRINT
    
    EXIT:
    MOV AH,4CH
    INT 21H
           


MAIN ENDP
    END MAIN